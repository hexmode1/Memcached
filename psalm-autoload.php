<?php

global $wgVersion, $wgAutoloadClasses, $IP;
$IP = getenv( "MW_INSTALL_PATH" );

if ( $IP === false ) {
	$IP = realpath( "../.." );
}
if ( $IP === false || !is_file( "$IP/includes/AutoLoader.php" ) ) {
	echo "Set the environment variable MW_INSTALL_PATH!\n";
	exit;
}

require_once "$IP/includes/AutoLoader.php";
