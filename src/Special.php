<?php
/**
 * A Memcached extension for MediaWiki
 * Originally written for ZeWiki
 * Provides an interface for checking if memcached is working fine
 *
 * @link https://www.mediawiki.org/wiki/Extension:Memcached Documentation
 * @link https://www.mediawiki.org/wiki/Extension_talk:Memcached Support
 *
 * Copyright (C) 2020  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Mark A. Hershberger <mah@nichework.com>
 * @license   GPL-3.0-or-later
 *
 * Original extension:
 * @author    UA2004 <ua2004 at ukr.net> for ZeWiki.com
 * @copyright Copyright (C) 2013, UA2004
 * @license   GPL-2.0-or-later
 */
namespace MediaWiki\Extension\Memcached;

use MemcachedClient;
use Message;
use ObjectCache;
use RequestContext;
use SpecialPage;
use Xml;

class Special extends SpecialPage {
	protected const MEMC_RELOAD = 2;
	protected const MEMC_OK = 1;
	protected const MEMC_ERROR = 0;
	protected const MEMC_NOT_FOUND = -1;

	public function __construct() {
		parent::__construct( 'Memcached' );

		// This is a debugging page.  I don't know why we would want any other context.
		$this->mContext = RequestContext::getMain();
	}

	/**
	 * @param ?string $subPage
	 * @return void
	 */
	public function execute( $subPage ) {
		$this->setHeaders();

		// @psalm-suppress DeprecatedFunction
		if ( !$this->getUser()->isAllowed( 'memcached' ) ) {
			$this->displayRestrictionError();
			return;
		}

		$memCachedServers = $this->getConfig()->get( "MemCachedServers" );
		if ( empty( $memCachedServers ) ) {
			$this->getOutput()->addHTML(
				'<h3>' . ( new Message( 'memcached-servers-not-set' ) )->text() . '</h3>'
			);
		} else {
			$this->getOutput()->addHTML( Xml::openElement( 'table', [ 'border' => 1 ] ) );

			foreach ( $memCachedServers as $server ) {
				$color = $message = "";
				switch ( $this->testMemcachedServer( $server ) ) {
				case self::MEMC_OK:
					$message = ( new Message( 'memcached-works' ) )->text();
					$color = '#84eb82';
					break;
				case self::MEMC_RELOAD:
					$message = ( new Message( 'memcached-reload' ) )->text();
					$color = '#aaaaff';
					break;
				case self::MEMC_ERROR:
					$message = ( new Message( 'memcached-not-working' ) )->text();
					$color = '#ffde46';
					break;
				case self::MEMC_NOT_FOUND:
					$message = ( new Message( 'memcached-not-found' ) )->text();
					$color = '#fe7f7a';
					break;
				}
				$this->getOutput()->addHTML(
					Xml::openElement( 'tr', [ 'style' => 'background-color:' . $color ] )
				);
				$this->getOutput()->addHTML( Xml::openElement( 'td' ) );
				$this->getOutput()->addHTML( $server );
				$this->getOutput()->addHTML( Xml::closeElement( 'td' ) );
				$this->getOutput()->addHTML( Xml::openElement( 'td' ) );
				$this->getOutput()->addHTML( $message );
				$this->getOutput()->addHTML( Xml::closeElement( 'td' ) );
				$this->getOutput()->addHTML( Xml::closeElement( 'tr' ) );
			}
			$this->getOutput()->addHTML( Xml::closeElement( 'table' ) );
		}
	}

	/**
	 * @param string $server
	 * @psalm-return -1|0|1|2
	 * @return int
	 */
	public function testMemcachedServer( string $server ): int {
		$memcache = new MemcachedClient( [
			'servers' => [ $server ],
			'debug' => true,
			'persistent' => false
		] );

		// @FIXME! This is a private method access
		$isMemcacheAvailable = $memcache->sock_to_host( $server );

		if ( $isMemcacheAvailable ) {
			$key = ObjectCache::getLocalClusterInstance()->makeKey(
				'zewiki', 'special', 'memcached', 'test'
			);
			$aData = [
				'me' => 'you',
				'us' => 'them',
			];
			$bData = $memcache->get( $key );
			if ( $bData === false ) {
				$memcache->set( $key, $aData, 60 );
				$bData = $memcache->get( $key );
				if ( $aData === $bData ) {
					return self::MEMC_RELOAD;
				} else {
					return self::MEMC_ERROR;
				}
			} elseif ( $aData === $bData ) {
				return self::MEMC_OK;
			}
		}
		return self::MEMC_NOT_FOUND;
	}

	/**
	 * @return string
	 */
	protected function getGroupName(): string {
		return 'wiki';
	}
}
